# STRipy
## Overview
STRipy is an application built for genotyping all short tandem repeats loci in the simplest way, especially the known pathogenic ones. At the moment, STRipy allows to genotype all known disease-causing STRs loci, in total of 58 diseases caused by 55 loci in 52 genes. A locus in each gene has been validated with series of simulations. In addition, STRipy supports custom loci as well that also allows to genotype VNTRs (e.g. CSTB, EIF4A3, PRNP and VWA1). Please see more at [stripy.org/about](https://stripy.org/about). 

STRipy uses [ExpansionHunter](https://github.com/Illumina/ExpansionHunter) as the genotyping engine and [REViewer](https://github.com/Illumina/REViewer) to create alignment visualisations. However, STRipy includes several additions to advance genotyping and diagnosing of diseases, such as:
* determining the actual read alignments and creating a "personalised variant catalogue" for each sample which enables to genotype alleles where the repeated sequence is longer than the fragment length (that is, usually above few hundred bases)
* including only fully repeated reads from off-target regions which are solely made of the pathogenic repeat unit (allowing maximum of one substitution) to provide better estimation of long alleles (i.e. to exclude unrelevant reads from off-target regions)
* counting and reporting the number of pathogenic repeats in spanning and flanking reads, specifically aiming to improve diagnosing diseases caused by _replaced_ and/or _nested_ type of repeats, such as recently discovered familial adult myoclonic epilepsies, CANVAS, spinocerebellar ataxia 31 and 37, but it is also useful to determine interruptions in short alleles
* using realignment to a complete sequence when genotyping XYLT1 locus which enables to accurately genotype the locus
* providing population data for each locus acquired from 2,504 individuals from 1000 Genomes Project cohort, stratified by superpopulations to assist in determining the potential pathogenicity of the repeat for the individual under analysis

To genotype a sample, you only need to select the disease or locus/gene to target (or write coordinates manually), provide an aligned and indexed short-read sequencing file (BAM or CRAM) and specify the genome it is aligned on (or let the system to automatically detect it). STRipy will extract out reads of interest from the file, which then will be forwarded to the server (either Cloud or Local) for genotyping. STRipy does not collect or store any data about the samples analysed in the Cloud longer than the genotyping takes place and does not receive the name of your BAM/CRAM file name (as this will be anonymised in your computer beforehand). The Standard analysis allows to genotype sample in a fast way, by only using reads in and near the targeted STR locus and therefore, genotypes are restricted to the fragment length. The Extended option enables to use reads from off-target regions as well and estimate genotypes above the fragment length, but increases the length of an analysis.

## Running the application
### System requirements
STRipy is a graphical application and requires a operating system with a GUI installed. The easiest way to run STRipy is to use pre-compiled binaries which contains all required libraries and therefore, nothing else has to be installed. Few extra steps (described below) are required when running STRipy under Windows 10/11 through WSL. When running STRipy from source code, Python 3.8+ is required with libraries specified in [requirements.txt](./requirements.txt) file (see instructions below). We have measured that the average RAM usage of STRipy version 1.1 is 120-130 MB. When using the Cloud, an internet connection is required and on average 400-500 KB of data transfer takes place when genotyping a sample.

### Easy way (by using compiled binaries)
It is very easy to run STRipy by using the pre-compiled binaries downloadable from [stripy.org](https://stripy.org). Please see the installation instructions for your operating system below.

#### macOS
Download the [compressed file](https://stripy.org/download/STRipy-v2.0-macOS.zip) and unpack the file (double-click on file). Drag STRipy.app to the Applications folder. Open STRipy from the Applications folder.

_NB! When running the application for the first time you will get a security warning saying this app cannot be opened because the developer cannot be verified. Click "Cancel" to close the window and then go to "System Preferences", click on "Security & Privacy" and under the "General" tab (at the bottom) it should say: "STRipy was blocked from use because it is not from an identified developer". Click "Open Anyway" and also "Open" on the pop-up window._

_If instead of STRipy there is a name of another application, then you need to make a choice for this one to remove the notification and display the one related to STRipy._

#### Linux
Download the [compressed file](https://stripy.org/download/STRipy-v2.0-Linux.zip). Unpack the file and then open Terminal, go to that folder and run STRipy by executing: `./STRipy`

If you want to do everything through command line, then please run the following commands:

`wget https://stripy.org/download/STRipy-v2.0-Linux.tar.gz`

`tar -xzvf STRipy-v2.0-Linux.tar.gz`

`./STRipy`

#### Windows 10/11
It is also possible to run the Linux version of STRipy in Windows 10/11 through WSL.

To do that, there are few steps that has to be taken (only once). First of all, set up WSL in Windows 10/11 and install a Linux distribution (such as Ubuntu): [https://docs.microsoft.com/en-us/windows/wsl/install](https://docs.microsoft.com/en-us/windows/wsl/install)

Please note that the installed Linux is a minimal OS without a graphical interface and it is missing the libraries required to run STRipy. Therefore, in order to successfully run STRipy, you need to install Xming X Server for Windows from [https://sourceforge.net/projects/xming/](https://sourceforge.net/projects/xming/) which will enable launching graphical applications.

Next, when you have opened the installed Linux distribution (a new window has opened where you can enter commands), you need to enable Web content engine library for GTK. To install and enable this, please run:

`sudo apt update`

`sudo apt install gir1.2-webkit2-4.0`

Download STRipy and unpack:

`wget https://stripy.org/download/STRipy-v2.0-Linux.tar.gz`

`tar -xzvf STRipy-v2.0-Linux.tar.gz`

To run STRipy (for the first and every other time), at first open XLaunch app in Windows (Keep default options, i.e. select "Multiple Windows" and "Display number" leave to 0) and click Next -> select "Start no client" and click Next -> then check "No Access Control" and click Next and finally click Finish.

When using WSL 1, then run `export DISPLAY=:0` in WSL (Linux) terminal. In case of WSL 2, run `export DISPLAY=$(grep -m 1 nameserver /etc/resolv.conf | awk '{print $2}'):0.0`. Finally launch the app: `./STRipy`

### Run from source
You can also run the application from the source code. In order to do that, first make sure you have [Python 3.8+](https://www.python.org/downloads/) in your system. Then download STRipy from git repository `git clone https://gitlab.com/andreassh/stripy-client.git` and go to the downloaded folder `cd stripy-client/`.

Install all required packages by executing: `python3 -m pip install -r requirements.txt`. In additional, you might also need to run: `python3 -m pip install pywebview[qt]`.

When all required packages are installed, you can run the application through terminal by simply executing: `python3 stri.py`

#### Compile from source code
You can compile the application from source code on your own. Please see the instructions for your operating system below.

##### macOS
To create the application for macOS from source, firstly install py2app library for Python: `python3 -m pip install py2app` and then in the stripy-client folder run `python3 create-macOS-app.py py2app` (STRipy.app will be saved into the "dist" folder, which then needs to be dragged to the Applications folder).

##### Linux and Windows 10/11
To create the executable file that is suitable for using in Linux (natively or through WSL in Windows 10/11), firstly install py2installer library for Python: `python3 -m pip install pyinstaller` and then in the stripy-client folder run `pyinstaller stri.py --name STRipy --onefile --hidden-import pysam.libctabixproxies --add-data "assets:assets" --icon icon.icns --upx-dir=..\upx391w` (The executable file will be saved into the "dist" folder).


## Use Local or internal server instead of the Cloud
Scripts and instructions to run STRipy's server are available in [STRipy's Server repository](https://gitlab.com/andreassh/stripy-server)

## Notes on using STRipy
Please interpret your results carefully when using the Extended analysis and detecting very long alleles. It is very comples to estimate long alleles and some of them could be reported longer than they actually are.

The "Genotype", "Confidence interval" and "Reads consistent with the genotype" is determined by ExpansionHunter, as well as the "Coverage", "Read length" and "Fragment length" under the Extra section. Because ExpansionHunter also counts non-pathogenic motifs into reported genotype (even in cases when there is no pathogenic repeat unit present) then STRipy tackles this issue by specifically counting the pathogenic repeat units in spanning and flanking reads independently and displays the results under the "Highest number of repeats found in flanking reads" section.

Population data is from analysing over two and half thousand samples of DRAGEN reanalysis of 1000 Genomes. This is divided into five superpopulations which results can be separately seen when clicking on the superpopulation name on the left from plot. The population graph includes two vertical lines corresponding to the determined genotype and the confidence interval displayed on top. The thick line as x-axis is representing the data collected from the literature, where green is the healthy, orange intermediate, red pathogenic and grey marks the unknown range.

In the variant section on top, the displayed repeat unit is the commonly reported motif and if the motif used for analysis was different (such as due to being on other strand) then it is displayed in parentheses (always on positive strand). Reference coordinates are the ones used for analysis, which corresponds to the genome assembly chosen (or autodetected by STRipy).

The visualised reads (under the Alignment visualiser tab) can be saved into your computer by clicking the button "Read alignments" in the bottom of the window. This is in vector graphics and can be zoomed in without losing image quality. All results can be saved into your computer as PDF report by clicking the corresponding button bottom of the results window. The PDF report also includes visualised read alignments.

## Configuration of STRipy
If you want to either change the local server's IP address, flanking region length before and after the STR locus where reads are extracted out or switch on/off use of alternative contigs then click the small button on the bottom right corner to open the configuration window.

## Contact
To contact the author then please use the contact form on [this page](https://stripy.org/about).
