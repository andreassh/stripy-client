"""
	STRipy
	Copyright (C) 2021  Andreas Halman

	STRipy is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	STRipy is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with STRipy.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import json
import regex
import pysam
import base64
import hashlib
import webview
import tempfile
import requests
import numpy as np
import urllib.request
import urllib.parse
from urllib.parse import urljoin


__author__ = "Andreas Halman"
__contact__ = "dev@stripy.org"
__url__ = "https://stripy.org"
__copyright__ = "Copyright (C) 2021, Andreas Halman"
__license__ = "GPLv3"
__version__ = "2.0"

current_results = ''
config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'assets/config.json')

class Api:
	def loadConfiguration(self):
		with open(config_file, 'r') as f: config = json.load(f)
		config.update({
			'server_web': 'https://stripy.org', # STRipy's web address
			'server_remote': 'cloud.stripy.org:31337', # STRipy's Cloud IP and port
			'catalog_file': os.path.join(os.path.dirname(os.path.realpath(__file__)), 'assets/catalog.json'), # Locally stored catalogue's path
			'loci_with_close_tract': ["ARX_1", "ARX_2", "HOXA13_1", "HOXA13_2", "HOXA13_3"], # Loci which will have tract close to the other one and therefore could potentially return a false genotype
			'ot_edges_bp': 300, # How much to extend off-target regions in base pairs
			'min_req_seq_len_proportion': 0.98, # Fraction of how long a read compared to the average length has to be so it will be included into analysis of determining the number of repeats in flanking reads or fully repeated reads
			'min_req_str_locus_proportion': 0.5 # Fraction of how long a repeated region in a read has to be so it would be included into analysis of determining the number of repeats in flanking reads
		})
		return config

	def getSoftwareVersion(self):
		"""Provides the software version to the app

		Returns:
			str: STRipy's software version
		"""

		return f"STRipy v{__version__}"


	def getConfiguration(self):
		"""Provides configuration options to the app which can be used to change under settings

		Returns:
			dict: Configuration
		"""
		return self.loadConfiguration()


	def updateConfig(self, newconf):
		global config
		"""Updates the config file after a user has saved changes in the app

		Args:
			newconf (dict): New configuration

		Returns:
			dict: Response whether the change was successful or not
		"""
		if newconf:
			with open(config_file, 'w') as conf_file:
				json.dump(newconf, conf_file, indent = 4)
			
			config = self.loadConfiguration()
			
			return True
		else:
			return False


	def checkServerStatus(self, serveraddr):
		"""Try to connect to a server and see whether it is responding

		Args:
			serveraddr (str): Server's address

		Returns:
			bool: True if server was responding, False if connection was unsuccessful
		"""
		try:
			ping_addr = urljoin(serveraddr, "ping") # Server's address to send the ping request
			server_status = requests.get(ping_addr).json()

			return (server_status["running"])
		except:
			return False


	def getServerStatus(self):
		"""App requesting to check both Local and Cloud server's status

		Returns:
			dict: Local and Cloud servers' status
		"""

		servers_status = {}
		for server in ["local", "remote"]:
			server_addr = "http://" + config["server_" + server]
			server_response = self.checkServerStatus(server_addr)
			servers_status[server] = server_response

		return servers_status

	def getReferenceSequence(self, genome, coord, flanking_bp = 0):
		"""App requesting reference sequence for custom region

		Returns:
			dict: Reference sequence
		"""
		if flanking_bp > 20:
			return False
		try:
			addr = urljoin("http://" + config["server_remote"], "refsequence") # Server's address to send the ping request
			urlparams = {'genome': genome, "coordinates": coord, "flankinglen": flanking_bp}
			response = requests.get(addr, params = urllib.parse.urlencode(urlparams, safe=':')).json()
			response["FlankingLength"] = flanking_bp
			return response
		except:
			return False


	def updateLocalCatalog(self):
		"""Download the latest catalogue file from server when requested and overwrite the current one on disk

		Returns:
			bool: True if successful, False otherwise
		"""

		try:
			catalog_content = requests.request(method = 'GET', url = urljoin(config['server_web'], "catalog.json"))
			catalog_content.raise_for_status()
			with open(config['catalog_file'], 'w') as cat_file:
				cat_file.write(catalog_content.text)
				return True

		except requests.exceptions.HTTPError as e:
			return False


	def versiontuple(self, version_str):
		"""Converts software version number to tuple in order to compare versions

		Args:
			version_str (str): Software version as a string

		Returns:
			tuple: Software version as a tuple
		"""

		return tuple(map(int, (version_str.split("."))))


	def checkLatestVersions(self):
		"""Checks the latest version of the tool and catalogue online, iniated automatically when starting the app

		Returns:
			dict: Returns bool for both catalogue and software keys, True for has updates and False if there are no updates available
		"""

		has_updates = {}
		with urllib.request.urlopen(urljoin(config['server_web'], "version.json")) as url:
			latest_versions = json.loads(url.read().decode())
			
			catalog_md5 = hashlib.md5(open(config['catalog_file'], 'rb').read()).hexdigest()
			has_updates["catalog"] = True if catalog_md5 != latest_versions["catalog"] else False
			has_updates["software"] = True if self.versiontuple(latest_versions['software']) > self.versiontuple(__version__) else False

		return has_updates


	def readCatalogue(self, disease_name, locus_name):
		"""Load the catalogue on disk and return the data about the requested disease and locus (gene)

		Args:
			disease_name (str): Abbreviation of the disease
			locus_name (str): Locus name (in majority of cases gene's abbreviation)

		Returns:
			dict: Data from the catalogue about the locus matching with requested disease
		"""

		with open(config['catalog_file'], mode = 'r') as cat_file:
			catalog = json.load(cat_file)

			for entry in catalog:
				if entry['Locus'] == locus_name:
					return entry
				
				for key, val in entry['Diseases'].items():
					if key == disease_name:
						return entry


	def createCatalogforCustomLocus(self, coordinates, motif, genome):
		"""Create custom catalogue entry for a custom targeted locus

		Args:
			coordinates (str): Genomic coordinates (e.g. chr1:100-200)
			motif (str): Motif (e.g. CAG)
			genome (str): Genome (e.g. hg38)

		Returns:
			dict: Custom catalogue entry
		"""
		entry = {
			"Locus": "Custom",
			"LocationCoordinates": {
				genome: coordinates
			},
			"Motif": motif,
			"MotifPlusStrand": motif,
			"RepeatType": "Standard",
			"Diseases": {
				"NA": {
					"DiseaseSymbol": "NA",
					"DiseaseName": "NA",
					"DiseaseOMIM": "NA",
					"Inheritance": "NA",
					"NormalRange": "NA",
					"IntermediateRange": "NA",
					"PathogenicCutoff": "NA"
				}
			},
			"OfftargetRegions": {},
			"FlankingSequences": {
				"Before": "",
				"After": ""
			},
			"GenomeExceptions": "",
			"PopulationData": {}
		}

		return entry

	def getListOfLociAndDiseases(self):
		"""Provide the list of loci/Loci and diseases which are displayed in the app's disease/locus menu

		Returns:
			dict: Dictionary containing two lists, one for displaying information in locus/gene selection menu and the other for diseases selection menu
		"""

		selection_list = {'Loci': [],
						  'Diseases': []
						 }
		tmp_dis = []
		with open(config['catalog_file'], mode = 'r') as cat_file:
			catalog = json.load(cat_file)

			for entry in catalog:
				selection_list['Loci'].append({'Locus': entry['Locus'],
												'Motif': entry['Motif'],
												'MotifPlusStrand': entry['MotifPlusStrand'],
												'LocationRegion': entry['LocationRegion'],
												'LocationCoordinates': entry['LocationCoordinates']
											   })

				for key, val in entry['Diseases'].items():
					if key not in tmp_dis:
						selection_list['Diseases'].append({	'Locus': entry['Locus'],
															'LocationCoordinates': entry['LocationCoordinates'],
															'MotifPlusStrand': entry['MotifPlusStrand'],
															'DiseaseSymbol': val['DiseaseSymbol'],
															'DiseaseName': val['DiseaseName']
															})
						tmp_dis.append(key)

		selection_list["Loci"].sort(key=lambda e: e['Locus'])
		selection_list["Diseases"].sort(key=lambda e: e['DiseaseName'])

		return selection_list


	def estimateReadFragSize(self, bamfile, alignments = 1000):
		"""Estimate the median size of reads and fragments

		Args:
			bamfile (file): BAM file
			alignments (int, optional): How many alignments to iterate through. Defaults to 1000.

		Returns:
			int: Median read length
			int: Median fragment length
		"""

		samfile = pysam.AlignmentFile(bamfile, 'rb')

		inserts = np.array([read.tlen for read in samfile.head(alignments) if read.tlen > 0])
		frag_median = int(np.median(inserts)) if len(inserts) > 0 else 0

		reads = np.array([read.query_length for read in samfile.head(alignments)])
		read_median = int(np.median(reads)) if len(reads) > 0 else 0

		return read_median, frag_median


	def determineRegionOverlap(self, reference, offtarget):
		"""Determine whether one region overlaps the other (such as the reference region and one of the off-target ones)

		Args:
			reference (array): Coordinates of the reference's start (int) and end position (int)
			offtarget (array): Coordinates of the off-target start (int) and end position (int)

		Returns:
			bool: Returns True if two regions overlap and False if not
		"""

		overlap = max(0, min(reference[2], offtarget[2]) - max(reference[1], offtarget[1]))

		if reference[0] == offtarget[0] and overlap > 0:
			return True
		else:
			return False


	def mergeOfftargets(self, offtargets_list):
		"""Merge overlapping (off-target) regions

		Args:
			offtargets_list (array): List of off-target regions to be merged

		Returns:
			array: List of merged overlapping off-target regions
		"""

		offtargets = []
		offtargets_list = [i for i in offtargets_list if i[0]]

		 # Exclude alternate contigs if set True in config (needed when reads are aligned on different genome assembly than on the server as there will be an error with ExpansionHunter while trying to access regions that are not in the provided reference genome)
		if config["use_alt_contigs"] == False:
			temp_ot_contigs = [i[0] for i in offtargets_list if "_" not in i[0]]
		else:
			temp_ot_contigs = [i[0] for i in offtargets_list]

		temp_ot_contigs = list(set(temp_ot_contigs))

		for contig in temp_ot_contigs:
			list_ot = []
			for pos in offtargets_list:
				if pos[0] == contig:
					pos1_positive = pos[1] if pos[1] > 0 else 0 # Also check that start position is not negative, if it is then set the start position as zero
					list_ot.append([pos1_positive, pos[2]])

			list_ot.sort(key = lambda interval: interval[0])
			merged = [list_ot[0]]

			for current in list_ot:
				previous = merged[-1]
				if current[0] <= previous[1]:
					previous[1] = max(previous[1], current[1])
				else:
					merged.append(current)

			for region in merged:
				offtargets.append(str(contig) + ':' + str(region[0]) + '-' + str(region[1]))

		return offtargets


	def convertToComplementarySequence(self, seq):
		"""Create reverse complementary DNA sequence

		Args:
			seq (str): DNA sequence

		Returns:
			str: Reverse complementary DNA sequence
		"""

		seq = seq[::-1]
		new_seq = ''

		for nucleotide in seq:
			if nucleotide == 'A':
				new_seq += 'T'
			elif nucleotide == 'T':
				new_seq += 'A'
			elif nucleotide == 'G':
				new_seq += 'C'
			elif nucleotide == 'C':
				new_seq += 'G'

		return new_seq


	def calculateRepeatedReads(self, motif, sequence, rep_type):
		"""Calculate the number of repeats in a sequence

		Args:
			motif (str): Repeat unit
			sequence (str): DNA (read) sequence
			rep_type (str): Repeat type (Standard, Imperfect GCN or Replaced/Nested)

		Returns:
			int: Number of repeat unit matches in the sequence
		"""

		# If Imperfect GCN then replace the N with selection of nucleotides used in the formula for matching motifs with regex
		if rep_type == "ImperfectGCN":
			imperfect_motif = motif.replace("N", "[A|T|G|C]")
			motif_to_find = f"({imperfect_motif})"
		else:
			motif_to_find = motif

		matches = len(regex.findall(motif_to_find, sequence))

		return matches


	def matchSequence(self, section, whole):
		"""Determine whether one sequence (flanking end) matches with another sequence (a whole read)

		Args:
			section (str): DNA sequence to match (flanking end)
			whole (str): Whole DNA sequence (read) where to look the match

		Returns:
			(match object): Regex match object
		"""

		# Allow maximum of 1 insertion, 1 deletion, 1 substitution and 1 error in total
		return regex.search("(" + section + "){i<=1,d<=1,s<=1,e<=1}", whole)


	def isFullyRepeatedRead(self, read_seq, motif_pathogenic, read_length):
		"""Determines whether read sequence is made solely of repeats or not. Allowing one insertion/deletion and two substitutions in the sequence.

		Args:
			read_seq (str): DNA (read) sequence
			motif_pathogenic (str): Pathogenic repeat unit
			read_length (int): Median read length

		Returns:
			bool: True if read is fully made of repeats, otherwise False
		"""
		if len(read_seq) > read_length*config['min_req_seq_len_proportion']:
			longseq = (round(read_length/len(motif_pathogenic)) * motif_pathogenic) + motif_pathogenic*2 # Create a long sequence which would be in the length of the average read plus two additional motifs
			longseq_comp = self.convertToComplementarySequence(longseq)

			return True if regex.search("(" + read_seq + "){i<=0,d<=0,s<=1,e<=1}", longseq) or regex.search("(" + read_seq + "){i<=0,d<=0,s<=1,e<=1}", longseq_comp) else False # Allow no INDELs, only 1 substitution (seq error)
		else:
			return False


	def calculateRepeatsInFlankingReads(self, seq, flanking_before, flanking_after, motif_pathogenic, repeat_type, read_length):
		"""Calculates the number of repeats found in flanking reads

		Args:
			seq (str): DNA (read) sequence
			flanking_before (str): Flanking DNA sequence before the STR locus
			flanking_after (str): Flanking DNA sequence after the STR locus
			motif_pathogenic (str): Pathogenic repeat unit
			repeat_type (str): Repeat type (Standard, Imperfect GCN or Replaced/Nested)
			read_length (int): Median read length

		Returns:
			(int): Number of repeat unit matches in the flanking reads
		"""

		if flanking_before and flanking_after:
			flanking_before_comp = self.convertToComplementarySequence(flanking_after)
			flanking_after_comp = self.convertToComplementarySequence(flanking_before)
			motif_pathogenic_comp = self.convertToComplementarySequence(motif_pathogenic)

			# Check the flanking sequence before and after the STR region and determine whether it matches with the reference one
			forward = True if self.matchSequence(flanking_before, seq) or self.matchSequence(flanking_after, seq) else False
			reverse = True if self.matchSequence(flanking_before_comp, seq) or self.matchSequence(flanking_after_comp, seq) else False

			# If sequence is on the opposite strand then switch and flip over the flanking ends
			if reverse:
				flanking_before = flanking_before_comp
				flanking_after = flanking_after_comp
				motif_pathogenic = motif_pathogenic_comp

			if forward or reverse:
				seq_flanking_before = self.matchSequence(flanking_before, seq)
				seq_flanking_after = self.matchSequence(flanking_after, seq)

				str_start = seq_flanking_before.end() if seq_flanking_before else 0
				str_end = seq_flanking_after.start()+1 if seq_flanking_after else len(seq)

				if str_start > 0 and str_end < len(seq): # Selecting only spanning reads
					return "spanning", self.calculateRepeatedReads(motif_pathogenic, seq[str_start:str_end], repeat_type)
				else: # Selecting only flanking reads
					# Check whether the read's sequence length is least X of the median read length (e.g. at least 135 bp in case of 150 bp reads, including the flanking start region)
					# Secondly check whether the repeated region is at least Y of the read's length
					if len(seq) > read_length*config['min_req_seq_len_proportion'] and str_start < read_length*config['min_req_str_locus_proportion']:
						return "flanking", self.calculateRepeatedReads(motif_pathogenic, seq[str_start:str_end], repeat_type)
					else:
						return False, False
			else:
				return False, False
		else:
			return False, False


	def selectSeqFile(self):
		"""Open the dialog box in app to select an aligned sequencing file for analysis

		Returns:
			str: Sample's file path
		"""

		file_types = ('Sequencing file (*.bam;*.cram)', 'All files (*.*)')
		file_path = window.create_file_dialog(webview.OPEN_DIALOG, allow_multiple = False, file_types = file_types)

		return file_path


	def valid_read(self, read):
		"""Check if a read is properly mapped

		Args:
			read (array): Read array

		Returns:
			bool: True if read has good mapping quality, otherwise False
		"""
		if read.mapping_quality >= 40 and read.reference_end and read.reference_start is not None:
			return True
		else:
			return False

	def autodetectSex(self, seq_file, genome):
		"""Use SRY gene's coding region's coverage to autodetect sample's sex

		Args:
			seq_file (file): BAM file
			genome (str): Genome

		Returns:
			str: Male if there are reads on SRY coding region
		"""
		SRY_hg19 = 'chrY:2655030-2655644'
		SRY_hg38 = 'chrY:2786989-2787603'
		SRY_coord = SRY_hg19 if genome == "hg19" else SRY_hg38

		try:
			file = pysam.AlignmentFile(seq_file, 'rb')
			genome_has_chr = True if "chr" in file.get_reference_name(0) else False

			if genome_has_chr == False:
				SRY_coord = SRY_coord.replace("chr", "")

			sryreads = sum([self.valid_read(read) for read in file.fetch(region = SRY_coord)])
			sex = "male" if sryreads > 10 else "female" # Use 10 reads as threshold to determine sample as male, otherwise female

		except:
			sex = False
			
		return sex


	def autodetectGenome(self, seq_file, disease_data):
		"""Autodetect the genome assembly

		Args:
			seq_file (file): Sample BAM or CRAM file
			disease_data (dict): Variant information about the locus being targeted, such as the flanking end sequences which are used to match sequences next to the STR locus

		Returns:
			str: Genome that is likely the one used to align samples on, if it could not be detected then returns False
		"""

		input_file = pysam.AlignmentFile(seq_file, 'rb')
		genome_has_chr = True if "chr" in input_file.get_reference_name(0) else False
		genome_hits = {"hg38": "", "hg19": ""}

		for gen, hits in genome_hits.items():
			chrom, start, end = regex.split(':|-', disease_data['LocationCoordinates'][gen])

			if genome_has_chr == False:
				chrom = chrom.replace("chr", "")

			flanking_match_counts_total = 0
			flanking_match_counts_hit = 0

			flanking_before = disease_data["FlankingSequences"]["Before"]

			if gen == "hg19" and "hg19" in disease_data["GenomeExceptions"]:
				if "FlankingSequences" in disease_data["GenomeExceptions"]["hg19"]:
					flanking_before = disease_data["GenomeExceptions"]["hg19"]["FlankingSequences"]["Before"]

			for read in input_file.fetch(chrom, int(start)-1-len(flanking_before), int(start)-1):
				if not read.is_reverse:
					flanking_match_counts_total += 1

					if self.matchSequence(flanking_before, read.query_sequence):
						flanking_match_counts_hit += 1

			genome_hits[gen] = flanking_match_counts_hit / flanking_match_counts_total if flanking_match_counts_total > 0 else 0

		gen_max = max(genome_hits, key = genome_hits.get)

		if genome_hits[gen_max] >= 0.2:
			return gen_max
		else:
			return False


	def makeBAM(self, seq_file, analysis_type, genome, tmpbam, disease_data):
		"""Create the analysis ready BAM file in a temporary folder and extract all reads relevant to the STR locus and determine off-target regions

		Args:
			seq_file (file): Original sample's BAM or CRAM sequence file
			analysis_type (str): Type of the analysis - either 'standard' or 'extended'
			genome (str): Detected or specified genome assembly
			tmpbam (file): Analysis ready BAM file in a temporary folder
			disease_data (dict): Variant information about the locus being targeted

		Returns:
			file: Analysis ready BAM file
			str: Highest number of repeats found in flanking with the count
			int: Number of fully repeated mates
			array: List of off-target regions in case of Extended analysis, otherwise returns an empty array
			str: Detected or specified genome assembly
			bool: Returns True if genome uses 'chr' in their chromosome names, otherwise False
		"""

		input_file = pysam.AlignmentFile(seq_file, 'rb')
		output_file = pysam.AlignmentFile(tmpbam, 'wb', template = input_file)

		# Work on the data now
		chrom, start, end = regex.split(':|-', disease_data['LocationCoordinates'][genome]) # Reference coordinates

		# Search contigs that includes chromosome names and then determine whether genome has "chr" in its contigs names, if not then remove "chr" from the reference coordinates
		contig_found = 0
		contig_id = 0

		while contig_found < 1:
			contig_name = input_file.get_reference_name(contig_id)

			if bool(regex.search(r'^chr[1-9]$|^chr1[0-9]$|^chr2[0-2]$|^chrX$|^chrY$|^[1-9]$|^1[0-9]$|^2[0-2]$|^X$|^Y$', contig_name, regex.MULTILINE)):
				contig_found = 1

				if "chr" not in contig_name:
					chrom = chrom.replace("chr", "")
					genome_has_chr = False
				else:
					genome_has_chr = True

			else:
				contig_id += 1

		# Define the start and end positions of the whole reference section that is extracted out
		section_min_start_pos = int(start) - config['fregion_length_bp']
		section_max_start_pos = int(end) + config['fregion_length_bp']

		# Define the start and end positions for fragments where one read would fall into the STR reference region 
		read_length, fragment_size = self.estimateReadFragSize(seq_file)
		fragment_min_start_pos = int(start) - (fragment_size-read_length)
		fragment_max_end_pos = int(end) + (fragment_size-read_length)

		offtargets = []
		offtargets_list = []
		offtarget_read_names = []
		offtarget_mate_positions = []
		path_repeats_in_spanning_reads = [] # Number of pathogenic repeats in spanning reads
		path_repeats_in_flanking_reads = [] # Number of pathogenic repeats in flanking reads
		path_repeats_in_mate_reads = [] # Number of pathogenic repeats in mates
		read_names = [] # Read names found near the STR locus
		mate_positions = [] # Genomic locations of mates

		motif_pathogenic = disease_data["MotifPlusStrand"]
		flanking_before = disease_data["FlankingSequences"]["Before"]
		flanking_after = disease_data["FlankingSequences"]["After"]
		repeat_type = disease_data["RepeatType"]
		locus = disease_data["Locus"]

		# Check whether there are any exceptions for hg19 genome and if yes then replace values
		if genome == "hg19" and "hg19" in disease_data["GenomeExceptions"]:
			if "FlankingSequences" in disease_data["GenomeExceptions"]["hg19"]:
				flanking_before = disease_data["GenomeExceptions"]["hg19"]["FlankingSequences"]["Before"]
				flanking_after = disease_data["GenomeExceptions"]["hg19"]["FlankingSequences"]["After"]

			if "MotifPlusStrand" in disease_data["GenomeExceptions"]["hg19"]:
				motif_pathogenic = disease_data["GenomeExceptions"]["hg19"]["MotifPlusStrand"]

		# Get the coverage
		cov_flanking_size = 300 # bp
		cov_start_pos = int(start) - cov_flanking_size 
		cov_end_pos = int(end) + cov_flanking_size

		region_coverage = input_file.count_coverage(chrom, cov_start_pos, cov_end_pos, quality_threshold = 15) # Set min quality score 15

		coverage_combined = np.array([region_coverage[0], region_coverage[1], region_coverage[2], region_coverage[3]]) # All nucleotides added together
		coverage_combined_sum = coverage_combined.sum(axis = 0).tolist()
		coverage_combined_sum = ', '.join([str(elem) for elem in coverage_combined_sum])

		# Get relevant reads to the locus
		process_read1_data = False

		for read in input_file.fetch(chrom, section_min_start_pos, section_max_start_pos):
			if locus == "XYLT1":
				if read.is_read1: # If locus is XYLT1 then get only read pairs
					output_file.write(read)
					process_read1_data = True
			else:
				if read.is_read1 or read.is_read2:
					output_file.write(read)
					process_read1_data = True

			if process_read1_data:
				read_type, read_repeats = self.calculateRepeatsInFlankingReads(read.query_sequence, flanking_before, flanking_after, motif_pathogenic, repeat_type, read_length)
				if read_repeats != False and read_repeats != None:
					if read_type == "spanning":
						path_repeats_in_spanning_reads.append(read_repeats)
					elif read_type == "flanking":
						path_repeats_in_flanking_reads.append(read_repeats)

				read_names.append(read.query_name)
				mate_positions.append([read.next_reference_name, read.next_reference_start, read.next_reference_start+read_length])

		# Get mates of reads that are in or overlapping the STR region
		merged_mate_positions = self.mergeOfftargets(mate_positions)
		merged_mate_positions.sort()

		for pos in merged_mate_positions:
			pos_chrom, pos_start, pos_end = regex.split(':|-', pos)

			for read in input_file.fetch(contig = pos_chrom, start = int(pos_start), stop = int(pos_end)):
				if locus == "XYLT1":
					if (read.is_read1 and read.query_name not in read_names) or (read.is_read2 and read.query_name in read_names): # Get the other read for XYLT1
						output_file.write(read)
				else:
					if read.query_name in read_names:
						output_file.write(read)

						if self.isFullyRepeatedRead(read.query_sequence, motif_pathogenic, read_length): # If the mate is fully repeated one then store this in the 'mates full of repeats' array
							path_repeats_in_mate_reads.append(read.query_name)
					else:
						continue

		# Secondly (if user wants to use off-target regions), get all reads which are so close to the STR region that their mates will map there and which could then be aligned on some other region --> make a list of these off-target regions
		if analysis_type == "extended":
			# Get all reads which are close to the repeated reference region and which mates would fall into the repeated region (based on the calculated fragment and read length)
			for read in input_file.fetch(chrom, fragment_min_start_pos, fragment_max_end_pos):
				if not read.mate_is_unmapped:
					try:
						# Get coordinates for Level 1 off-target regions
						ot1_chrom = read.next_reference_name
						ot1_start = int(read.next_reference_start) - config['ot_edges_bp']
						ot1_end = ot1_start + read_length + (config['ot_edges_bp']*2)

						if self.determineRegionOverlap([chrom, section_min_start_pos, section_max_start_pos], [ot1_chrom, ot1_start, ot1_end]): # If the region is overlapping then don't add that
							continue

						offtargets_list.append([ot1_chrom, ot1_start, ot1_end])
					except:
						continue

			# If there are elements in off-target list then merge overlapping L1 off-target regions and sort the list
			if offtargets_list:
				offtargets = self.mergeOfftargets(offtargets_list)

				# Get all reads in L1 off-target regions and their mates
				for region in offtargets:
					ot_chrom, ot_start, ot_end = regex.split(':|-', region)

					for ot1_read in input_file.fetch(ot_chrom, int(ot_start), int(ot_end)):
						if ot1_read.query_name not in read_names: # If it is mate of a read that is next to the STR region and not already in file, then check whether it is fully repeated and if yes, then get it
							if self.isFullyRepeatedRead(ot1_read.query_sequence, motif_pathogenic, read_length):
								output_file.write(ot1_read)

								offtarget_read_names.append(ot1_read.query_name) # Put L1 read names into a list
								offtarget_mate_positions.append([ot1_read.next_reference_name, ot1_read.next_reference_start, ot1_read.next_reference_start+read_length]) # And their coordinates as well
							else:
								continue

				merged_offtarget_mate_positions = self.mergeOfftargets(offtarget_mate_positions)
				merged_offtarget_mate_positions.sort()

				# Extract out mates of L1 reads
				for ot2_pos in merged_offtarget_mate_positions:
					ot2_pos_chrom, ot2_pos_start, ot2_pos_end = regex.split(':|-', ot2_pos)
	
					for ot2_read in input_file.fetch(contig = ot2_pos_chrom, start = int(ot2_pos_start), stop = int(ot2_pos_end)):
						if ot2_read.query_name in offtarget_read_names and ot2_read.query_name not in read_names: # Take the read if it is the mate of a read from L1 region and not the STR locus because this has been extracted out already
							if self.isFullyRepeatedRead(ot2_read.query_sequence, motif_pathogenic, read_length):
								output_file.write(ot2_read) # Extract out only if it is full of repeats

		path_repeats_combined = path_repeats_in_spanning_reads + path_repeats_in_flanking_reads # Combine spanning and flanking reads
		path_repeats_in_mate_reads = sorted(set(path_repeats_in_mate_reads)) # Take unique values (i.e. filter duplicate read names out)

		# Set 0-s if no elements
		path_repeats_combined = path_repeats_combined if len(path_repeats_combined) > 0 else 0
		return_repeats_mates = len(path_repeats_in_mate_reads) if path_repeats_in_mate_reads else 0

		# Determine the number of pathogenic repeats in flanking reads
		if path_repeats_combined:
			path_repeats_combined_highest = {}

			for repeats in path_repeats_combined:
				number_of_reads = path_repeats_combined.count(repeats)
				path_repeats_combined_highest[str(repeats)] = number_of_reads
		else:
			path_repeats_combined_highest = {"0": 0} # Set zeros if no results

		path_repeats_combined_highest = dict(sorted(path_repeats_combined_highest.items(), key=lambda x: x[1], reverse = True)[:7]) # Get the 7 maximum values to display on the app
		highest_path_repeats_combined = str(path_repeats_combined_highest)
		return_repeats_combined = str(highest_path_repeats_combined).replace("'", '"') # Need to do the ' -> " replace for javascript to be able to convert it to object

		offtargets = offtargets if analysis_type == "extended" else [] # Only return off-target regions when doing the Extended analysis

		# Close files
		input_file.close()
		output_file.close()
		
		return tmpbam, return_repeats_combined, return_repeats_mates, offtargets, genome, genome_has_chr, coverage_combined_sum


	def downloadFileDialog(self, file_name):
		"""Opens a file save dialog for the app, either to save read visualisation file or PDF report

		Args:
			file_name (str): File name from app, which also contains the name of the original file

		Returns:
			dialog: Save file dialog
		"""

		return window.create_file_dialog(webview.SAVE_DIALOG, directory = '', save_filename = file_name)


	def downloadFileSaveBinaryContent(self, file_path, req_file):
		"""Save content to a specified file

		Args:
			file_path (str): File path where to save content
			req_file (str): Type of the PDF file requested by user
		"""

		if req_file == 'pdf-report':
			file_content = current_results["Files"]["PDFreport"]
			
		elif req_file == 'read-alignments':
			file_content = current_results["Files"]["PDFreadsvis"]


		if file_path != "null" and file_content:
			base64_bytes = file_content.encode('utf-8')

			with open(file_path, "wb") as out_file:
				decoded_pdf_data = base64.decodebytes(base64_bytes)
				out_file.write(decoded_pdf_data)

		return True


	def runAnalysis(self, req_disease, req_locus, req_custom, req_sex, req_genome, req_file, req_analysis, req_server):
		"""Main function which will iniate the analysis

		Args:
			req_disease (str): User specified name of the disease, or empty when a Locus was selected
			req_locus (str): User specified name of the locus/gene, or empty when a Disease was selected
			req_custom (list): User specified custom coordinates and motif
			req_sex (str): User specified sex of the sample, or autodetect; default is female
			req_genome (str): User specified genome assembly, or autodetect
			req_file (file): Original user's sample BAM or CRAM file
			req_analysis (str): User's requested type of the analysis - either 'standard' or 'extended
			req_server (str): Server to send the response, either 'local' or 'remote' (Cloud)

		Returns:
			dict: Returned response to the Client
		"""

		global current_results
		global config

		# Check whether sequencing file has been indexed, if not return an error
		req_file_index = req_file+'.crai' if req_file.endswith('cram') else req_file+'.bai' # e.g. file.bam.bai

		if not os.path.isfile(req_file_index): # If file.bam.bai does not exist, try another naming for index (e.g. file.bai)
			req_file_index = os.path.splitext(req_file)[0]+'.crai' if req_file.endswith('cram') else os.path.splitext(req_file)[0]+'.bai' # e.g. file.bai

		if not os.path.isfile(req_file_index):
			error = json.dumps({"Error": True, "ErrorMessage": "Index for the sequencing file was not found. Cannot proceed."})
			response = {'data': error}
			return response

		selected_server_url = config['server_local'] if req_server == 'local' else config['server_remote']
		config.update({
			'query_url': urljoin('http://' + selected_server_url, 'analyse'), # Temporary update the config and set the selected server where analysis will be conducted
		})

		# Limit region length to 200 bp for loci with close tracts, otherwise it is the default 1000 bp
		extracted_region_length = 200 if config['fregion_limitclosetracts'] and req_locus in config['loci_with_close_tract'] else 1000

		# Check and verify custom locus info
		custom_coordinates = req_custom[0].lower()
		custom_coordinates = custom_coordinates.replace("x", "X")
		custom_coordinates = custom_coordinates.replace("y", "Y")
		custom_coordinates = custom_coordinates.replace(",", "")
		custom_coordinates = "chr"+custom_coordinates if not custom_coordinates[:3] == "chr" else custom_coordinates

		custom_motif = req_custom[1].upper()

		use_custom = True if custom_coordinates and custom_motif else False
		custom_error = False

		if use_custom:
			if bool(regex.search(r'[^CATGN]+', custom_motif)): # If motif contains non-nucleotide symbols
				custom_error = True

			if bool(regex.search(r'^\w.*:\d.*-\d.*', custom_coordinates)): # If coordinates are not written correctly
				custom_chrom, custom_start, custom_end = regex.split(':|-', custom_coordinates)
				custom_start = int(custom_start)
				custom_end = int(custom_end)

				if not bool(regex.search(r'^chr[1-9]$|^chr1[0-9]$|^chr2[0-2]$|^chrX$|^chrY$|^[1-9]$|^1[0-9]$|^2[0-2]$|^X$|^Y$', custom_chrom, regex.MULTILINE)): # If chromosome name is not correct
					custom_error = True
			
				if not custom_start > config['fregion_length_bp'] or not custom_end > custom_start or not custom_end-custom_start <= 200: # If start is not above the flanking region length and end is not past start and the region size is above 200 bp
					custom_error = True
			else:
				custom_error = True

		if custom_error: # Make sure we have correct chromosome notation and that coordinates are not empty and the regions is less than 200 bp
			error = json.dumps({"Error": True, "ErrorMessage": "Incorrect reference coordinates. Make sure you have written the coordinates correctly (including chromosome name) and that the targeted STR region is less than 200 bp long."})
			response = {'data': error}
			return response

		catalog_entry = self.readCatalogue(req_disease, req_locus) if not use_custom else self.createCatalogforCustomLocus(custom_coordinates, custom_motif, req_genome)
		
		# When user is requesting to autodetect the genome assembly, then try to do it and display an error if it fails (only works with known pathogenic loci)
		if req_genome == "autodetect" and catalog_entry:
			if use_custom:
				error = json.dumps({"Error": True, "ErrorMessage": "Cannot use autodetect for manually specified locus."})
				response = {'data': error}
				return response

			detected_genome = self.autodetectGenome(req_file, catalog_entry)

			if detected_genome:
				req_genome = detected_genome
			else:
				error = json.dumps({"Error": True, "ErrorMessage": "Genome assembly could not be detected. Please specify it manually."})
				response = {'data': error}
				return response

		# When user is requesting to autodetect the sample's sex
		if req_sex == "autodetect":
			detected_sex = self.autodetectSex(req_file, req_genome)

			if detected_sex:
				req_sex = detected_sex
			else:
				error = json.dumps({"Error": True, "ErrorMessage": "Sex could not be detected. Please specify it manually."})
				response = {'data': error}
				return response
		
		bamfile = tempfile.NamedTemporaryFile(suffix = '.bam').name
		baifile = None
		
		try:
			with open(bamfile, 'w') as tmp:
				bam_for_analysis, repeats_in_flanking, repeats_in_mates, offtarget_list, genome, genome_has_chr, region_coverage = self.makeBAM(req_file, req_analysis, req_genome, tmp, catalog_entry)

				bam_file_size = os.stat(bam_for_analysis.name).st_size / 1024 # Generated BAM file size in kilobytes

				# Sort and index the generated BAM file and forward the content to Server along with some metadata
				pysam.sort('-o', bamfile, bamfile)
				pysam.index(bamfile)
				baifile = bamfile + '.bai'
				content_bam = open(bamfile, 'rb')
				content_bai = open(baifile, 'rb')

				files = [('file', content_bam), 
						 ('file', content_bai)]
				
				metadata = {'software_version': __version__,
							'disease': req_disease,
							'locus': req_locus if not use_custom else "Custom", # Set locus name custom if specified a custom one
							'custom_entry': json.dumps(catalog_entry) if use_custom else '',
							'sex': req_sex,
							'genome': genome,
							'genome_chr': genome_has_chr,
							'server': req_server,
							'analysis': req_analysis,
							'limit_region': extracted_region_length,
							'region_coverage': region_coverage,
							'repeats_flanking': repeats_in_flanking,
							'repeats_mates': repeats_in_mates,
							'offtargets': json.dumps(offtarget_list) if (req_analysis == "extended" and len(offtarget_list) > 0) else ''}

				try:
					if bam_file_size < 10: # If the generated BAM file is less than 10 kb then it means it does not contain any reads
						error = json.dumps({"Error": True, "ErrorMessage": "No reads found in the selected locus. Cannot proceed with the analysis."})
						response = {'data': error}
					else:
						req = requests.post(config['query_url'], files = files, data = metadata) # Get the response from the Server and return it to the Client
						current_results = json.loads(req.text) # Put the results into a temporary string
						results_to_return = ({key:current_results[key] for key in current_results if key != 'Files'}) # Remove Files section (where PDFs are stored) before sending the results to the JS

						response = {'data': json.dumps(results_to_return)}

				finally:
					content_bam.close()
					content_bai.close()
		finally:
			os.remove(bamfile)
			os.remove(baifile)

		return response


if __name__ == '__main__':
	"""Starts the app and creates the GUI window
	"""

	api = Api()
	config = api.loadConfiguration()
	window = webview.create_window('STRipy', 'assets/gui.html', js_api = api, width = 960, height = 760, resizable = False)
	webview.start(debug=True)